package com.gitlab.caluml;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class ParsedUrl {

  private String protocol;
  private String host;
  private int port;
  private String path;
  private Map<String, String> params;
  private String fragment;

  public ParsedUrl(String protocol,
                   String host,
                   int port,
                   String path,
                   Map<String, String> params,
                   String fragment) {
    this.protocol = protocol;
    this.host = host;
    this.port = port;
    this.path = path;
    this.params = params;
    this.fragment = fragment;
  }

  public ParsedUrl() {
    params = new HashMap<>();
  }

  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    this.protocol = protocol;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Map<String, String> getParams() {
    return params;
  }

  public void setParams(Map<String, String> params) {
    this.params = params;
  }

  public String getFragment() {
    return fragment;
  }

  public void setFragment(String fragment) {
    this.fragment = fragment;
  }

  public void removeParameter(String parameter) {
    params.remove(parameter);
  }

  public void setParameter(String key,
                           String value) {
    params.put(key, value);
  }

  public String getUrl() {
    if (protocol != null) {
      return protocol + "://" + host + formatPort() + path + formatParams() + formatFragment();
    } else {
      return "//" + host + formatPort() + path + formatParams() + formatFragment();
    }
  }

  private String formatPort() {
    if (port == 0) return "";
    for (Protocol protocol : Protocol.values()) {
      if (port == protocol.getPort() && this.protocol.equals(protocol.name().toLowerCase())) {
        return "";
      }
    }

    return ":" + port;
  }

  private String formatParams() {
    StringJoiner joiner = new StringJoiner("&");
    for (Map.Entry<String, String> e : params.entrySet()) {
      final String key = e.getKey();
      final String value = e.getValue();
      if (value == null) {
        joiner.add(key);
      } else {
        String s = key + "=" + value;
        joiner.add(s);
      }
    }
    String paramString = joiner.toString();
    if (paramString.isEmpty()) {
      return "";
    }
    return "?" + paramString;
  }

  private String formatFragment() {
    if (fragment == null) return "";
    return "#" + fragment;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ParsedUrl.class.getSimpleName() + "[", "]")
      .add("protocol='" + protocol + "'")
      .add("host='" + host + "'")
      .add("port=" + port)
      .add("path='" + path + "'")
      .add("params=" + params)
      .add("fragment='" + fragment + "'")
      .toString();
  }
}
