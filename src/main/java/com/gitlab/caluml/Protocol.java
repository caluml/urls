package com.gitlab.caluml;

public enum Protocol {

  HTTP(80),
  HTTPS(443),
  FTP(21);

  private final int port;

  Protocol(int port) {
    this.port = port;
  }

  public int getPort() {
    return port;
  }
}
