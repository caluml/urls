package com.gitlab.caluml;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Urls {

  private static Pattern portPattern = Pattern.compile("\\:([0-9]+)");

  public static ParsedUrl parse(String url) {
    String protocol = getProtocol(url);
    String host = getHost(url);
    int port = getPort(url);
    String path = getPath(url);
    Map<String, String> params = getParams(url);
    String fragment = getFragment(url);

    return new ParsedUrl(protocol, host, port, path, params, fragment);
  }

  private static String getProtocol(String url) {
    return url.substring(0, url.indexOf("://"));
  }

  private static String getHost(String url) {
    int firstSlashes = url.indexOf("://");
    int portColon = url.indexOf(":", firstSlashes + 3);
    int hostEnd = url.indexOf("/", firstSlashes + 3);
    if (portColon != -1) {
      hostEnd = portColon;
    }

    return url.substring(firstSlashes + 3, hostEnd);
  }

  private static int getPort(String url) {
    Matcher matcher = portPattern.matcher(url);
    if (matcher.find()) {
      return Integer.parseInt(matcher.group(1));
    }
    String protocol = getProtocol(url);
    if (protocol.equals("https")) return 443;
    if (protocol.equals("ftp")) return 21;
    return 80;
  }

  private static String getPath(String url) {
    int firstSlashes = url.indexOf("://");
    int nextSlashes = url.indexOf("/", firstSlashes + 3);
    int questionMark = url.indexOf("?");
    if (questionMark == -1) {
      return url.substring(nextSlashes);
    }

    return url.substring(nextSlashes, questionMark);
  }

  private static Map<String, String> getParams(String url) {
    Map<String, String> map = new HashMap<>();
    int questionMark = url.indexOf("?");
    if (questionMark == -1) return map;

    int hash = url.indexOf("#");

    int endParams = hash != -1 ? hash : url.length();
    String paramString = url.substring(questionMark + 1, endParams);

    String[] params = paramString.split("&");
    for (String param : params) {
      final String[] split = param.split("=");
      final String key = split[0];
      String value = null;
      if (split.length == 2) {
        value = split[1];
      }
      map.put(key, value);
    }

    return map;
  }


  private static String getFragment(String url) {
    if (url.contains("#")) {
      return url.substring(url.indexOf("#") + 1);
    }

    return null;
  }

}


