package com.gitlab.caluml;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ParsedUrlTest {

  private ParsedUrl parsedUrl;

  @Before
  public void before() {
    Map<String, String> params = new HashMap<>();
    params.put("this", "that");
    params.put("limit", "123");
    parsedUrl = new ParsedUrl("https", "google.com", 443, "/dir1/dir2", params, "fragment");
  }

  @Test
  public void Can_change_scheme_in_ParsedUrl() {
    parsedUrl.setProtocol("ftp");

    assertThat(parsedUrl.getUrl()).isEqualTo("ftp://google.com:443/dir1/dir2?this=that&limit=123#fragment");
  }

  @Test
  public void Can_change_host_in_ParsedUrl() {
    parsedUrl.setHost("facebook.co.uk");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://facebook.co.uk/dir1/dir2?this=that&limit=123#fragment");
  }

  @Test
  public void Can_change_port_in_ParsedUrl() {
    parsedUrl.setPort(1234);

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com:1234/dir1/dir2?this=that&limit=123#fragment");
  }

  @Test
  public void Can_change_path_in_ParsedUrl() {
    parsedUrl.setPath("/newdir/newdir2");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/newdir/newdir2?this=that&limit=123#fragment");
  }

  @Test
  public void Can_change_fragment_in_ParsedUrl() {
    parsedUrl.setFragment("new-fragment");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#new-fragment");
  }

  @Test
  public void Can_remove_fragment_in_ParsedUrl() {
    parsedUrl.setFragment(null);

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123");
  }

  @Test
  public void Can_add_parameter_in_ParsedUrl() {
    parsedUrl.setParameter("place", "Bristol");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123&place=Bristol#fragment");
  }

  @Test
  public void Can_remove_parameter_in_ParsedUrl() {
    parsedUrl.removeParameter("this");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?limit=123#fragment");
  }

  @Test
  public void Can_set_path_in_ParsedUrl() {
    parsedUrl.setPath("/");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/?this=that&limit=123#fragment");
  }

//  @Test
//  public void Can_remove_path_in_ParsedUrl() {
//    parsedUrl.setPath(null);
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/?this=that&limit=123#fragment");
//  }

  @Test
  public void Can_build_ParsedUrl_from_scratch() {
    ParsedUrl parsedUrl = new ParsedUrl();
    parsedUrl.setProtocol("https");
    parsedUrl.setHost("google.com");
    parsedUrl.setPath("/dir1/dir2");
    parsedUrl.setParameter("this", "that");
    parsedUrl.setParameter("limit", "123");
    parsedUrl.setFragment("fragment");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }
//
//  @Test
//  public void Can__in_ParsedUrl() {
//
//    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/dir1/dir2?this=that&limit=123#fragment");
//  }

}
