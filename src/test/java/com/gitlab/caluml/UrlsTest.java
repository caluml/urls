package com.gitlab.caluml;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UrlsTest {

  @Test
  public void Can_parse_URL() {
    final ParsedUrl parsedUrl = Urls.parse("https://google.com:8443/dir1/dir2/foo.php?this=that#fragment");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com:8443/dir1/dir2/foo.php?this=that#fragment");
  }

  @Test
  public void Can_parse_URL_with_no_param_name() {
    final ParsedUrl parsedUrl = Urls.parse("https://google.com/?12345678&this=that");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://google.com/?this=that&12345678");
  }

  @Test
  public void Can_parse_non_ASCII_URL() {
    final ParsedUrl parsedUrl = Urls.parse("https://fa.wikipedia.org/wiki/%D9%85%DA%A9%D8%A7%D9%86%DB%8C%D8%A7%D8%A8_%D9%85%D9%86%D8%A8%D8%B9_%DB%8C%DA%A9%D8%B3%D8%A7%D9%86");

    assertThat(parsedUrl.getUrl()).isEqualTo("https://fa.wikipedia.org/wiki/%D9%85%DA%A9%D8%A7%D9%86%DB%8C%D8%A7%D8%A8_%D9%85%D9%86%D8%A8%D8%B9_%DB%8C%DA%A9%D8%B3%D8%A7%D9%86");
  }


  @Test
  public void Can_parse_URL_with_non_ASCII_domain() {
    final ParsedUrl parsedUrl = Urls.parse("http://www.例子.卷筒纸/");

    assertThat(parsedUrl.getUrl()).isEqualTo("http://www.例子.卷筒纸/");
  }
}
